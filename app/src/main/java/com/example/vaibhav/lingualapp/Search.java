package com.example.vaibhav.lingualapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.SearchView;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class Search extends AppCompatActivity {
    String quer;
    public SearchView searchView;
    Stocks[] stocks;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchView=(SearchView)findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Log.e("hka",query);
                quer=query;
                recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                new SearchFetcher().execute("");
            return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });



    }



    class SearchFetcher extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String[] params) {
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\n    \"query\":\""+quer+"\"\n}");
            Request request = new Request.Builder()
                    .url("http://sandbox.reverieinc.com/v2/search/stocks?lang=hindi")
                    .post(body)
                    .addHeader("rev-api-key", "pyrNIGm8k7kdhMgxCUYUtuNVbwYBJ9TNVg8h")
                    .addHeader("rev-app-id", "DemoAxis")
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "20152ca0-8d9c-c74d-1179-68e15fe9be11")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {

            if (response != null) {
                //   Log.e("TEST", response);
                //Trending[] temp = new GsonBuilder().create().fromJson(response, Trending[].class);

                try {

                    JSONObject obj =new JSONObject(response);
                    JSONArray arr = obj.getJSONArray("stocks");
                    Log.e("Test",arr.toString());
                    stocks=new Stocks[arr.length()];
                    for (int i = 0; i < stocks.length; i++) {


//                        String NSE_ISIN,
//                        String CompanyName_Lng,
//                        String BSE_ISIN



                        stocks[i]=new Stocks(arr.getJSONArray(i).getJSONObject(i).getString("NSE_ISIN")+"",
                                arr.getJSONArray(i).getJSONObject(i).getString("CompanyName_Lng")+"",
                                arr.getJSONArray(i).getJSONObject(i).getString("BSE")+""
                                );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                linearLayoutManager = new LinearLayoutManager(Search.this);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(linearLayoutManager);

                recyclerView.setAdapter(new RecycleImageAdapter(Search.this,stocks ));
                recyclerView.getAdapter().notifyDataSetChanged();





            }
        }
    }



}
