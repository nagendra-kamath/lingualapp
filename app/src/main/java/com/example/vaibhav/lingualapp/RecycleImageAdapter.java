package com.example.vaibhav.lingualapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Shekhar on 1/6/2016.
 */
public class RecycleImageAdapter extends RecyclerView.Adapter<RecycleImageAdapter.ViewHolder>
implements RecyclerView.OnItemTouchListener , RecyclerViewClickListener
{


    protected RecyclerView mRecyclerView;

    View v;
    public Bundle b = new Bundle();

    private Search mainActivity;
    private Stocks[] trends;

    public RecycleImageAdapter(Search mainActivity, Stocks[] dataset) {
        this.mainActivity = mainActivity;
        this.trends = dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int i) {
        v = LayoutInflater.from(mainActivity).inflate(R.layout.list_row, viewGroup, false);
        return new ViewHolder(v);
    }


    // Set CustomAdapter as the adapter for RecyclerView.




    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {


        //#6ba2a7cf
        //#6bc93c9e


        viewHolder.BSE_ISIN.setText("NSE_ISIN: "+trends[i].BSE_ISIN);
        viewHolder.NSE_ISIN.setText("BSE "+trends[i].NSE_ISIN);
        viewHolder.CompanyName_Lng.setText("CompanyName "+trends[i].CompanyName_Lng);


    }

    @Override
    public int getItemCount() {
        return trends.length;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
       return  false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    public int getItemID(int position) {
        return position;
    }



    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

    }





    public class ViewHolder extends RecyclerView.ViewHolder  {


        public TextView NSE_ISIN;
        public TextView CompanyName_Lng;
        public TextView BSE_ISIN;


        public ViewHolder(View itemView) {
            super(itemView);



            NSE_ISIN = (TextView) itemView.findViewById(R.id.NSE_ISIN);
            CompanyName_Lng = (TextView) itemView.findViewById(R.id.CompanyName_Lng);
            BSE_ISIN= (TextView) itemView.findViewById(R.id.BSE_ISIN);

        }


    }
}

interface RecyclerViewClickListener
{

   public void recyclerViewListClicked(View v, int position);
}