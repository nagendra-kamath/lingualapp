package com.example.vaibhav.lingualapp;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Welcome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

public Bundle b;
    String lang;
public TextView textView;
    public TextView textView2;
    public Button button;
    public Button b3;
    WifiManager wifi;
    String bal;
    String search_stock;
    String avail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Button b2=(Button)findViewById(R.id.button4);
        b3=(Button)findViewById(R.id.button5);
        search_stock=b3.getText().toString();

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);

            }
        });

        textView=(TextView)findViewById(R.id.textView2);
        textView2=(TextView)findViewById(R.id.textView3);
        bal=textView.getText().toString();
        bal=bal.split(" ")[1];
        Log.e("bal",bal+"");

        wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);

        if(!wifi.isWifiEnabled()){
            Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
            startActivity(i);
        }



        avail=textView.getText().toString();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        b=getIntent().getExtras();
        lang=b.getString("lang");


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Welcome.this,Search.class);
                Bundle b=new Bundle();
                b.putString("lang",lang);
                intent.putExtras(b);
                startActivity(intent);
            }
        });



        if(lang.equals("english")){


        }
        else if(lang.equals("hindi")){
            textView2.setText("₹"+"\"९९९९९\"");
        }

        else if(lang.equals("marathi")){
            textView2.setText("₹"+"\"९९९९९\"");
        }

        else if(lang.equals("gujarati")){
            textView2.setText("₹"+"૯૯૯૯૯");
        }


        new ValueFetcher().execute("");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    class ValueFetcher extends AsyncTask<String, Void, String> {
        @Override



        protected String doInBackground(String[] params) {
            try {

                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, "{\"data\" : [\""+avail+"\",\""+search_stock+"\"]}");
                Request request = new Request.Builder()
                        .url("http://sandbox.reverieinc.com/v2/transliteration?target_lang="+lang+"&source_lang=english&domain=19")
                        .post(body)
                        .addHeader("rev-api-key", "pyrNIGm8k7kdhMgxCUYUtuNVbwYBJ9TNVg8h")
                        .addHeader("rev-app-id", "DemoAxis")
                        .addHeader("content-type", "application/json")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("postman-token", "a1f9ec9b-b77b-8afa-3ac6-2aa3670dcd2d")
                        .build();

                Response response = client.newCall(request).execute();
                Log.e("Testok", response.toString());


                //Log.e("TEST",stringBuilder.toString());
                return response.body().string();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {

            if (response != null) {
                Log.e("TEST", response);


                try {
                    JSONObject obj =new JSONObject(response);
                    Log.e("shit",obj.getJSONObject("responseMap").getString(avail.toLowerCase()));
                    textView.setText(obj.getJSONObject("responseMap").getString(avail.toLowerCase()));
                   b3.setText(obj.getJSONObject("responseMap").getString(search_stock.toLowerCase()));

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
