package com.example.vaibhav.lingualapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LandingPage extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        final EditText editText = (EditText) findViewById(R.id.AccNo);
//        final TextView textView = (TextView) findViewById(R.id.TXT);
        final FriendsDBHelper helper = new FriendsDBHelper(this);

        String qw=helper.getAllPersons();
        Log.e("qw",qw);
        if(!qw.equals(""))
        {
            Intent intent = new Intent(LandingPage.this,MainActivity.class);
            startActivity(intent);
        }
        Button button = (Button) findViewById(R.id.Submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FriendsDBHelper.ACCOUNT_NUMBER = editText.getText().toString();
//                Context context;
                String temp = editText.getText().toString();
                Log.e("wtf", FriendsDBHelper.ACCOUNT_NUMBER + "");
                helper.DBinsert(temp);
                String we = helper.getAllPersons();
                Log.e("txt", we);
                Intent intent = new Intent(LandingPage.this,MainActivity.class);
                startActivity(intent);
//                editText.setText(we);
//                Cursor rs = FriendsDBHelper.getAllPersons();
//                String str = rs.getString(rs.getColumnIndex(FriendsDBHelper.ACCOUNT_NUMBER));
            }
        });
    }
}
