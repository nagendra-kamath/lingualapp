package com.example.vaibhav.lingualapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public  class FriendsDBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "SQLiteExample.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME="user";
    public static final String TABLE_ACCOUNT_NUMBER="accountnumber";
    public static String ACCOUNT_NUMBER="";
    private FriendsDBHelper helper;
    private SQLiteDatabase db;
    private Context context;

    public FriendsDBHelper(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
        this.context=context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                        TABLE_ACCOUNT_NUMBER + " TEXT)"
        );
    }
    public void LolCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                        TABLE_ACCOUNT_NUMBER + " TEXT)"
        );
    }
    public long DBinsert(String tmp){
        ContentValues values = new ContentValues();
        helper = new FriendsDBHelper(context);
        db = helper.getWritableDatabase();
        onUpgrade(this.db,db.getVersion(),db.getVersion()+1);
        Log.e("DATA", tmp);
        values.put(FriendsDBHelper.TABLE_ACCOUNT_NUMBER, tmp);
        return db.insert(FriendsDBHelper.TABLE_NAME, null ,values);
    }

    public String getAllPersons() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        String str ="";
        if(res.moveToFirst())
        {
            str=res.getString(res.getColumnIndex(FriendsDBHelper.TABLE_ACCOUNT_NUMBER));
            Log.e("hello",str);
            res.close();
        }
        return str;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void Upgrade(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
