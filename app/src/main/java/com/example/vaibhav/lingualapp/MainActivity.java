package com.example.vaibhav.lingualapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RelativeLayout l1;
    public TextView greeting;
    public String textString1;
    public String textString2;
    public String textString3;
    public String textString4;
    public Button v1;
    public Button v2;
    public Button v3;
    public String lang="english";
    public String prev=null;
    public Bundle b;
    int mainhindi;
    int mainguju;
    int mainmar;
    WifiManager wifi;
    SharedPreferences sharedPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        b=new Bundle();
        b.putString("lang","english");

        //mainhindi
        //mainguju
        //mainmar


         mainhindi=sharedPref.getInt("mainhindi",-1);
         mainguju=sharedPref.getInt("mainguju",-1);
         mainmar=sharedPref.getInt("mainmar",-1);




        if(sharedPref.getString("visited1","shie").equals("shie")&&!wifi.isWifiEnabled()){
            Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
            startActivity(i);
        }


        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("visited1", "value");
        editor.commit();


        Log.e("hyf",sharedPref.getString("key","shie"));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

         greeting=(TextView) findViewById(R.id.greeting);
        v1=(Button) findViewById(R.id.button);
        v2=(Button) findViewById(R.id.button2);
        v3=(Button) findViewById(R.id.button3);

        v1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("bc","df");
                Intent intent = new Intent(MainActivity.this,login2.class);
                intent.putExtras(b);
                startActivity(intent);

            }
        });

        textString1=greeting.getText().toString();
        textString2=v1.getText().toString();
        Log.e("but1",textString2);
        textString3=v2.getText().toString();
        textString4=v3.getText().toString();

        String xyz="http://sandbox.reverieinc.com/v2/transliteration?target_lang=hindi&source_lang=english&domain=19";
        new ValueFetcher().execute(xyz);
    }

    public void onResume(){
        super.onResume();  // Always call the superclass method first
        String xyz="http://sandbox.reverieinc.com/v2/transliteration?target_lang=hindi&source_lang=english&domain=19";
        new ValueFetcher().execute(xyz);
        // Get the Camera instance as the activity achieves full user focus

       sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);


//        if(sharedPref.getString("visited1","shie").equals("shie")&&!wifi.isWifiEnabled()){
//            Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
//            startActivity(i);
//        }


        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("visited1", "value");
        editor.commit();


        Log.e("hyf",sharedPref.getString("key","shie"));



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Resources res = getResources();
        String current = res.getConfiguration().locale.getCountry();
        Log.i("Current", current);
        String localeString = new String(current);
//        if (but.equals(butEn)) {
//            localeString = "en";
//        } else if (but.equals(butLt)) {
//            localeString = "lt";
//        } else if (but.equals(butRu)) {
//            localeString = "ru";
//        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.english) {


            prev=lang;
            lang="english";
            b.putString("lang","english");
            setprevlang();
        }

       else if (id == R.id.hindi) {
            lang="hindi";
            b.putString("lang","hindi");
            if(mainhindi==-1){
               if(!wifi.isWifiEnabled()){
                   Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
                   startActivity(i);
               }

                prev=lang;
                lang="hindi";

                new ValueFetcher().execute("ghb");
            }
            else{
                greeting.setText(sharedPref.getString("hintext1","shie"));
                v1.setText(sharedPref.getString("hintext2","shie"));
                v2.setText(sharedPref.getString("hintext3","shie"));
                v3.setText(sharedPref.getString("hintext4","shie"));
                b.putString("lang","hindi");
            }




        }

       else if (id == R.id.marathi) {
            lang="marathi";
            b.putString("lang","marathi");
            if(mainmar==-1){
                if(!wifi.isWifiEnabled()){
                    Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(i);
                }

                prev=lang;

                b.putString("lang","marathi");
                new ValueFetcher().execute("ghb");
            }
            else{
                greeting.setText(sharedPref.getString("martext1","shie"));
                v1.setText(sharedPref.getString("martext2","shie"));
                v2.setText(sharedPref.getString("martext3","shie"));
                v3.setText(sharedPref.getString("martext4","shie"));
            }

        }

       else if (id == R.id.gujarati) {
            lang="gujarati";
            b.putString("lang","gujarati");
            if(mainguju==-1){
                if(!wifi.isWifiEnabled()){
                    Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(i);
                }

                prev=lang;

                b.putString("lang","gujarati");
                new ValueFetcher().execute("ghb");
            }
            else{
                greeting.setText(sharedPref.getString("gujtext1","shie"));
                v1.setText(sharedPref.getString("gujtext2","shie"));
                v2.setText(sharedPref.getString("gujtext3","shie"));
                v3.setText(sharedPref.getString("gujtext4","shie"));
            }

        }

        else if(id==R.id.error){
            Intent in=new Intent(MainActivity.this,ReportError.class);
            startActivity(in);

        }



        Log.e("this lang",lang);

        return super.onOptionsItemSelected(item);
    }


    public void setprevlang(){

        greeting.setText(textString1);
        v1.setText(textString2);
        v2.setText(textString3);
        v3.setText(textString4);


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    class ValueFetcher extends AsyncTask<String, Void, String> {
        @Override



        protected String doInBackground(String[] params) {
            try {
//
                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, "{\"data\" : [\""+textString1+"\"]}");
                RequestBody body1 = RequestBody.create(mediaType, "{\"data\": [\""+textString1+"\",\""+textString2+"\",\""+textString3+"\",\""+textString4+"\"]}");


                Request request = new Request.Builder()
                        .url("http://sandbox.reverieinc.com/v2/localization?target_lang="+lang+"&source_lang="+prev)
                        .post(body1)
                        .addHeader("rev-api-key", "pyrNIGm8k7kdhMgxCUYUtuNVbwYBJ9TNVg8h")
                        .addHeader("rev-app-id", "DemoAxis")
                        .addHeader("content-type", "application/json")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("postman-token", "056c3745-3330-2db2-b563-0cceed1e8a1c")
                        .build();

                Response response = client.newCall(request).execute();
                Log.e("Testok", response.toString());


                //Log.e("TEST",stringBuilder.toString());
                return response.body().string();
            }
                catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {

            if (response != null) {
                   Log.e("TEST", response);

                String token;
                try {
               //     JSONArray arr = new JSONArray(response);
                    JSONObject obj =new JSONObject(response);
                    obj.put("data",textString1);

                    Log.e("Hel",obj.getJSONObject("responseMap").getString(textString1));
                    greeting.setText(obj.getJSONObject("responseMap").getString(textString1));
                    Log.e("Hel2",obj.getJSONObject("responseMap").getString(textString2));
                    Log.e("Hel3",obj.getJSONObject("responseMap").getString(textString3));
                    Log.e("Hel4",obj.getJSONObject("responseMap").getString(textString4));

                    v1.setText(obj.getJSONObject("responseMap").getString(textString2));
                    v2.setText(obj.getJSONObject("responseMap").getString(textString3));
                    v3.setText(obj.getJSONObject("responseMap").getString(textString4));

                    if(lang.equals("marathi")){


                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("martext1", obj.getJSONObject("responseMap").getString(textString1));
                        editor.putString("martext2", obj.getJSONObject("responseMap").getString(textString2));
                        editor.putString("martext3", obj.getJSONObject("responseMap").getString(textString3));
                        editor.putString("martext4", obj.getJSONObject("responseMap").getString(textString4));
                        editor.putInt("mainmar", 1 );
                        editor.commit();

                    }

                    if(lang.equals("gujarati")){


                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("gujtext1", obj.getJSONObject("responseMap").getString(textString1));
                        editor.putString("gujtext2", obj.getJSONObject("responseMap").getString(textString2));
                        editor.putString("gujtext3", obj.getJSONObject("responseMap").getString(textString3));
                        editor.putString("gujtext4", obj.getJSONObject("responseMap").getString(textString4));
                        editor.putInt("mainguju", 1 );
                        editor.commit();

                    }



                    if(lang.equals("hindi")){

                        Log.e("Main hindi mai aaya","Test");
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("hintext1", obj.getJSONObject("responseMap").getString(textString1));
                        editor.putString("hintext2", obj.getJSONObject("responseMap").getString(textString2));
                        editor.putString("hintext3", obj.getJSONObject("responseMap").getString(textString3));
                        editor.putString("hintext4", obj.getJSONObject("responseMap").getString(textString4));
                        editor.putInt("mainhindi", 1 );
                        editor.commit();
                        Log.e("SHit just got real",sharedPref.getInt("mainhindi",-9)+"");

                    }


                    }
                catch (JSONException e) {
                    e.printStackTrace();
                }

                }




            }
        }


    }




